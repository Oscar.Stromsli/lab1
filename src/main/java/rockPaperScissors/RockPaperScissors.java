package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {

    public static void main(String[] args) {
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    public String result;

    public void run() {

        while(true) {

            System.out.println("Let's play round " + roundCounter);
            Random random = new Random();
            String computerMove = rpsChoices.get(random.nextInt(rpsChoices.size()));
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if ((!humanMove.equals("rock")) && (!humanMove.equals("paper")) && (!humanMove.equals("scissors"))) {
                System.out.println("I do not understand " + humanMove + ". Try again.");
                continue;
            } else

            if (humanMove.equals(computerMove)) {
                result = "It's a tie!";
            } else if (HumanWins(humanMove, computerMove)) {
                result = "Human wins!";
                humanScore++;
            } else {
                result = "Computer wins";
                computerScore++;
            }

            System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". " + result);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continueQuestion = continuePlaying();
            if(continueQuestion.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            else {
                roundCounter++;
            }
        }
    }

    static boolean HumanWins(String HumanMove, String ComputerMove) {
        if (HumanMove.equals("rock")) {
            return ComputerMove.equals("scissors");
        }
        else if (HumanMove.equals("paper")) {
            return ComputerMove.equals("rock");
        } else {
            return ComputerMove.equals("paper");
        }
    }

    public String continuePlaying() {
        while(true) {
            String continueQuestion = readInput("Do you wish to continue playing? (y/n)?");
            if(continueQuestion.contains("y")) {
                return continueQuestion;
            }
            else if (continueQuestion.contains("n")) {
                return continueQuestion;
            } else {
                System.out.println("I do not understand " + continueQuestion + ". Try Again");
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}
